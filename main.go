package main

import (
	"fmt"
	"strconv"

	"github.com/pulumi/pulumi-openstack/sdk/v3/go/openstack/compute"
	"github.com/pulumi/pulumi-openstack/sdk/v3/go/openstack/images"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		nodecount, _ := strconv.Atoi(conf.Require("nodecount"))
		image_url := conf.Require("image_url")
		keypair, err := compute.NewKeypair(ctx, "janjan_pulumi", &compute.KeypairArgs{
			Name:      pulumi.String("janjan_pulumi"),
			PublicKey: pulumi.String("ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDyw2z/C+5YugYNXQXbeop0AcOjmWZCvcmci/vOAboO8 janjan@laptop"),
		})
		if err != nil {
			return err
		}
		ubuntu_image, err := images.NewImage(ctx, "ubuntu_focal", &images.ImageArgs{
			Name:            pulumi.String("ubuntu_focal"),
			ContainerFormat: pulumi.String("bare"),
			DiskFormat:      pulumi.String("qcow2"),
			ImageSourceUrl:  pulumi.String(image_url),
		})
		if err != nil {
			return err
		}
		opt0 := 1024
		opt1 := 1
		compute.LookupFlavor(ctx, &compute.LookupFlavorArgs{
			Ram:   &opt0,
			Vcpus: &opt1,
		}, nil)
		if err != nil {
			return err
		}
		for i := 0; i < nodecount; i++ {
			nodename := fmt.Sprintf("ubuntu-%d", i)
			instance, _ := compute.NewInstance(ctx, nodename, &compute.InstanceArgs{
				FlavorName: pulumi.String("ds1G"),
				ImageId:    ubuntu_image.ID(),
				//Name:       pulumi.String(fmt.Sprintf("ubuntu-%d", i)),
				Name:    pulumi.String(nodename),
				KeyPair: keypair.ID(),
				Networks: compute.InstanceNetworkArray{
					&compute.InstanceNetworkArgs{
						Name: pulumi.String("private"),
					},
				},
			})
			ctx.Export(fmt.Sprintf("%s-ip", nodename), instance.AccessIpV4)
		}

		ctx.Export("ubuntu_image_id", ubuntu_image.ID())
		return nil
	})
}
